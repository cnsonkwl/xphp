<?php
/**
 * @Description 
 * @Sonkwl Xiong
 * @Date 2024/11/08 15:11:54
 */

namespace Sonkwl\xphp;

use Sonkwl\xphp\Starter;

class Controller{
    //@Sonkwl Xiong 2024/11/08 15:12:58
    //@Desc 载入
    public static function load($c,$f){
        if(!is_file("controller/{$c}.php")){
            Starter::response("NG","处理文件不存在{$c}");
        }
        include_once "controller/{$c}.php";
        if(!method_exists($c,$f)){
            Starter::response("NG","功能不存在{$c}.{$f}");
        }
        call_user_func("{$c}::{$f}");
    }
}
?>