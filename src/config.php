<?php
namespace Sonkwl\xphp;

use Sonkwl\xphp\apper;

class config{
    private static $config=[];

    public static function Set($val){
        self::$config=$val;
    }
    public static function Get($key){
        if(!isset(self::$config[$key])){
            apper::Response($res="NG",$message="config/config.php中{$key}没有配置");
        }
        return self::$config[$key];
    }

    public static function GetPDO(){
        if(!isset(self::$config["host"],self::$config["dbname"],self::$config["user"],self::$config["pwd"])){
            apper::Response($res="NG",$message="config/config.php 没有定义host,dbname,user,pwd");
        }
        return new \PDO("mysql:host=".self::Get("host").";dbname=".self::Get("dbname"),self::Get("user"),self::Get("pwd"),array(\PDO::ATTR_EMULATE_PREPARES => false));
    }

    public static function Run(){
        if(!is_file("config/config.php")){
            apper::Response($res="NG",$message="config/router.php不存在");
        }
        include_once "config/config.php";
    }
}
?>