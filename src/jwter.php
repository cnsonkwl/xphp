<?php
namespace Sonkwl\xphp;

use Sonkwl\xphp\apper;

class jwter{
    static private $keystr='WHSS';
    static private $keyint=7;
    static private $version="V1";//jwt版本
    static private $exp="";

    static public function Set($key,$value){
        if(!isset(self::$$key)){
            apper::Response($res="NG",$message="系统没有定义参数{$key}");
        }
        self::$$key=$value;
    }
    static public function Get($key){
        return self::$$key;
    }

    //IHDS加密算法
	/*
	* $rInt操作权值，一般使用(可以自定义)：0:查询，1:新增，2:修改，3:删除，5:跨域验证,7:JWT
	*/
	static public function SetDoIHDS($string,$rInt)
	{
		$hex="";
		for($i=0;$i<strlen($string);$i++)
			$hex.=dechex(ord($string[$i])+$rInt);
		return $hex.$rInt.date("iHds");
	}
	//IHDS解密算法
	static public function GetDoIHDS($hex,$rInt)
	{   
		$t_hex=substr($hex,0,strlen($hex)-strlen($rInt.date("iHds")));
		$string=""; 
		for($i=0;$i<strlen($t_hex)-1;$i+=2)
			$string.=chr(hexdec($t_hex[$i].$t_hex[$i+1])-$rInt);
		return $string;
	}

    /**
     * JWT 版本V1
    */
    //生成jwt
    static public function SetJWTV1($id){
        $token=$id.'-'.self::Get("exp").'-'.md5(self::Get("keystr").$id.self::Get("exp"));
        $token=self::SetDoIHDS($token,self::Get("keyint"));
        return $token;
    }
    //验证jwt
    static public function CheckJWTV1($token){
        $tstr=self::GetDoIHDS($token,self::Get("keyint"));
        $tokenarray=explode('-',$tstr);

        if(count($tokenarray)!=3 || $tokenarray[1]<time() || $tokenarray[2]!=md5(self::Get("keystr").$tokenarray[0].$tokenarray[1])){
            apper::Response($res="NG",$message="token验证失败或已经失效V1");
        }

        return $tokenarray;
    }

    /**
     * JWT 版本V2
    */
    //生成jwt
    static public function SetJWT($id){
        $token='{"id":"'.$id.'","exp":"'.self::Get("exp").'","md5":"'.md5(self::Get("keystr").$id.self::Get("exp")).'"}';
        $token=self::SetDoIHDS($token,7);
        return $token;
    }
    //验证jwt
    static public function CheckJWT($token){
        //echo self::$keystr;
        $tstr=self::GetDoIHDS($token,7);
        $tokenarray=(array)json_decode($tstr,true);
        if(count($tokenarray)!=3 || $tokenarray["exp"]<time() || $tokenarray["md5"]!=md5(self::Get("keystr").$tokenarray["id"].$tokenarray['exp'])){
            apper::Response($res="NG",$message="token验证失败或已经失效V2");
        }
        return $tokenarray;
    }

    //验证函数
    static public function GetVersion(){
        return self::Get("version")=="V1"?"V1":"";
    }

    //返回id
    static public function Validate($key){
        $token=false;
        if(isset($_GET[$key])){
            $token=$_GET[$key];
        }
        if(isset($_POST[$key])){
            $token=$_POST[$key];
        }
        if(!$token){
            apper::Response($res="NG",$message="没有定义token参数");
        }

        $tokenArr=call_user_func("self::CheckJWT".self::GetVersion(),$token);
        return self::Get("version")=="V1"?$tokenArr[0]:$tokenArr["id"];
    }

    static public function SetToken($str){
        return call_user_func("self::SetJWT".self::GetVersion(),$str);
    }
}
?>