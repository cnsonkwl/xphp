<?php
/**
 * @Description 
 * @Sonkwl Xiong
 * @Date 2024/11/08 15:11:54
 */

namespace Sonkwl\xphp;

use Sonkwl\xphp\Starter;

class Auther{
    //@Sonkwl Xiong 2024/11/08 15:12:58
    //@Desc 载入
    public static function load($m,$f){
        if(is_file("auther/{$m}_{$f}.php")){
            include_once "auther/{$m}_{$f}.php";
            return 0;
        }
        if(is_file("auther/default.php")){
            include_once "auther/default.php";
        }
    }
}
?>