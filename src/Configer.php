<?php
namespace Sonkwl\xphp;

use Sonkwl\xphp\Starter;

class Configer{
    private static $config=[];

    public static function init($val){
        self::$config=$val;
    }
    public static function set($k,$v){
        self::$config[$k]=$v;
    }
    public static function check($key){
        if(!isset(self::$config[$key])){
            return false;
        }
        return true;
    }
    public static function get($key){
        if(!isset(self::$config[$key])){
            Starter::response("NG","config/config.php中{$key}没有配置");
        }
        return self::$config[$key];
    }

    public static function getPDO(){
        if(!isset(self::$config["host"],self::$config["dbname"],self::$config["user"],self::$config["pwd"])){
            Starter::response("NG","config/config.php 没有定义host,dbname,user,pwd");
        }
        return new \PDO("mysql:host=".self::Get("host").";dbname=".self::Get("dbname"),self::Get("user"),self::Get("pwd"),array(\PDO::ATTR_EMULATE_PREPARES => false));
    }

    public static function load(){
        if(!is_file("configer/config.php")){
            Starter::response("NG","configer/config.php不存在");
        }
        include_once "configer/config.php";
    }
}
?>