<?php
/**
 * @Description 
 * @Sonkwl Xiong
 * @Date 2023/10/09 11:32:27
 * @GET 
 * 
 * @POST 
 * 
 * @Response 
 *  
 */
namespace Sonkwl\xphp;
class mailer{

    public static $to=NULL;
    public static $subject=NULL;
    public static $content=NULL;
    public static $cc=NULL;
    public static $bcc=NULL;
    public static $url=NULL;
    public static $vKey=NULL;//唯一值

    public static function Set($key,$value){
        if($key=="to" && is_null($value)){
            echo "收件人为空";
            exit;
        }
        if($key=="subject" && is_null($value)){
            echo "主题为空";
            exit;
        }
        if($key=="url" && is_null($value)){
            echo "url为空";
            exit;
        }
        self::$$key=$value;
    }
    public static function Get($key){
        if($key=="vKey" && self::$$key==""){
            return 'system';
        }
        return self::$$key;
    }

    public static function GetContext(){
        $val=["to","subject","content","cc","bcc"];
        $info=[];
        foreach ($val as $key => $value) {
            $info[$value]=self::Get($value);
        }
        return stream_context_create(array(
            'http' => array(  
                'method' => 'POST',  
                'header' => 'Content-type:application/x-www-form-urlencoded',
                'content' => http_build_query($info), 
                'timeout' => 30
            )  
        ));
    }

    public static function Send(){
        // self::GetContext();
        $result = file_get_contents(self::Get("url"), false, self::GetContext());
        echo $result;
        // echo "[".date("Y-m-d H:i:s")."]".self::Get("to")."\r\n";
    }
}
?>