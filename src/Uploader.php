<?php
/**
 * @Description
 * @Sonkwl Xiong
 * @Date 2024/11/08 15:11:54
 */

namespace Sonkwl\xphp;

use Sonkwl\xphp\Starter;
use Sonkwl\xphp\Configer;

class Uploader{

    static $types=[
        "image/png"=>"png",
        "image/jpg"=>"jpg",
        "image/jpeg"=>"jpeg",
        "image/gif"=>"gif",
        "application/pdf"=>"pdf",
        "application/vnd.ms-excel"=>"xls",
        "application/vnd.ms-powerpoint"=>"doc",
        "application/msword"=>"ppt",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"=>"xlsx"
    ];
    
    static $size=20;

    static $tag="default";

    static $path="files";
    static $url="./";

    //@Sonkwl Xiong 2024/11/08 15:12:58
    //@Desc 载入
    public static function load($m,$f){
        // * 是否配置
        if(Configer::check("upload_types")){
            self::$types=Configer::get("upload_types");
        }
        if(Configer::check("upload_size")){
            self::$size=Configer::get("upload_size");
        }
        if(Configer::check("upload_path")){
            self::$path=Configer::get("upload_path");
        }
        if(Configer::check("upload_tag")){
            self::$tag=Configer::get("upload_tag");
        }
        if(Configer::check("upload_url")){
            self::$url=trim(Configer::get("upload_url"),"/");
        }
        
        if(is_file("uploader/{$m}_{$f}.php")){
            include_once "uploader/{$m}_{$f}.php";
            return 0;
        }

        if(is_file("uploader/default.php")){
            include_once "uploader/default.php";
        }
    }

    //@Sonkwl Xiong 2024/11/08 16:27:27
    //@Desc $_FILES/filedata 转 POST
    public static function toPost($k){
        // * 检查保存目录是否存在
        if(!is_dir(self::$path)){
            mkdir(self::$path,0777);
        }

        if(isset($_FILES[$k])){
            $_POST[$k]=self::saveByArray($_FILES[$k]);
        }
        if(isset($_POST[$k])){
            $_POST[$k]=self::saveByData($_POST[$k]);
        }
    }

    //* 文件类型/大小判断，返回文件保存路径
    public static function getFilePath($type,$size){
        // * 类型判断
        $types=[];
        foreach (self::$types as $key => $val) {
            array_push($types,$key);
        }
        if(!in_array($type,$types)){
            Starter::response("NG","上传文件格式错误，支持".implode(",",$types));
        }
        // * 文件大小判断
        if($size>self::$size*1024*1024){
            Starter::response("NG","文件大小不能超过".self::$size."M");
        }
        // * 保存文件名称
        return self::$path."/".self::$tag.date("YmdHis").random_int(1,10000).".".self::$types[$type];
    }

    //* 处理$_FILES
    public static function saveByArray($file){
        try {
            // * 保存文件
            $filePath=self::getFilePath($file["type"],$file["size"]);
            move_uploaded_file($file["tmp_name"],$filePath);

            // * 返回文件url路径
            return self::$url."/".$filePath;
        }catch (\Throwable $th) {
            Starter::response("NG","文件保存失败");
        }
    }
    
    //* 处理filedata
    public static function saveByData($file){
        preg_match('/data:(\w+?\/\w+?);base64,(.+)$/si',$file,$result);
        if(count($result)!=3){
            return $file;
        }

        // * 保存文件
        $filePath=self::getFilePath($result[1],intval(strlen($result[2]))/1024/1024);
        file_put_contents($filePath,base64_decode($result[2]));

        // * 返回文件url路径
        return self::$url."/".$filePath;
    }
}
?>