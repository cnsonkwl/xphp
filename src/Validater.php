<?php
/**
 * @Description
 * @Sonkwl Xiong
 * @Date 2024/11/08 15:11:54
 */

namespace Sonkwl\xphp;

use Sonkwl\xphp\Starter;

class Validater{
    //@Sonkwl Xiong 2024/11/08 15:12:58
    //@Desc 载入
    public static function load($m,$f){
        if(!is_file("validater/{$m}_{$f}.php")){
            Starter::response("NG","验证文件不存在{$m}_{$f}");
        }
        include_once "validater/{$m}_{$f}.php";
    }
}
?>