<?php
/**
 * @Description jwt处理
 * @Sonkwl Xiong
 * @Date 2024/11/12 11:10:48
 */
namespace Sonkwl\xphp;

use Sonkwl\xphp\Configer;
use Sonkwl\xphp\Starter;


class Jwtter{
    static $keystr='WHSS';
    static $keyint=7;
    static $version="V1";//jwt版本
    static $exp="";

    //* 载入参数
    public static function load(){
        if(Configer::check("jwt_keystr")){
            self::$keystr=Configer::get("jwt_keystr");
        }
        if(Configer::check("jwt_keyint")){
            self::$keyint=Configer::get("jwt_keyint");
        }
        if(Configer::check("jwt_version")){
            self::$version=Configer::get("jwt_version");
        }
        if(Configer::check("jwt_exp")){
            self::$exp=Configer::get("jwt_exp");
        }
        if(self::$exp==""){
            Starter::response("NG","jwtter必须定义过期时间exp");
        }
    }

	/*
    * IHDS加密算法
	* $rInt操作权值，一般使用(可以自定义[0-9])：0:查询，1:新增，2:修改，3:删除，5:跨域验证,7:JWT
	*/
	public static function setDoIHDS($string,$rInt){
		$hex="";
		for($i=0;$i<strlen($string);$i++)
			$hex.=dechex(ord($string[$i])+$rInt);
		return $hex.$rInt.date("iHds");
	}
	//* IHDS解密算法
	public static function getDoIHDS($hex,$rInt){
		$t_hex=substr($hex,0,strlen($hex)-strlen($rInt.date("iHds")));
		$string="";
		for($i=0;$i<strlen($t_hex)-1;$i+=2)
			$string.=chr(hexdec($t_hex[$i].$t_hex[$i+1])-$rInt);
		return $string;
	}

    //* JWT 版本V1
    //* 生成jwt
    public static function setJWTV1($id){
        $token=$id.'-'.self::$exp.'-'.md5(self::$keystr.$id.self::$exp);
        $token=self::setDoIHDS($token,self::$keyint);
        return $token;
    }
    //* 验证jwt
    public static function checkJWTV1($token){
        $tstr=self::getDoIHDS($token,self::$keyint);
        $tokenarray=explode('-',$tstr);

        if(count($tokenarray)!=3 || $tokenarray[1]<time() || $tokenarray[2]!=md5(self::$keystr.$tokenarray[0].$tokenarray[1])){
            Starter::response("NG","(V1)token验证失败或已经失效");
        }

        return $tokenarray;
    }

    //* JWT 版本V2
    //* 生成jwt
    public static function setJWT($id){
        $token='{"id":"'.$id.'","exp":"'.self::$exp.'","md5":"'.md5(self::$keystr.$id.self::$exp).'"}';
        $token=self::setDoIHDS($token,7);
        return $token;
    }
    public static function checkJWT($token){
        $tstr=self::getDoIHDS($token,7);
        $tokenarray=(array)json_decode($tstr,true);
        if(count($tokenarray)!=3 || $tokenarray["exp"]<time() || $tokenarray["md5"]!=md5(self::$keystr.$tokenarray["id"].$tokenarray['exp'])){
            Starter::response("NG","(V2)token验证失败或已经失效");
        }
        return $tokenarray;
    }

    //获得版本
    public static function getVersion(){
        return self::$version=="V1"?"V1":"";
    }

    //* 设置token
    public static function setToken($id){
        return call_user_func("self::SetJWT".self::getVersion(),$id);
    }

    //* 验证token,返回id
    public static function validate($key){
        $token=false;
        if(isset($_GET[$key])){
            $token=$_GET[$key];
        }
        if(isset($_POST[$key])){
            $token=$_POST[$key];
        }
        if(!$token){
            Starter::response("NG","GET/POST没有定义token参数");
        }
        $tokenArr=call_user_func("self::checkJWT".self::getVersion(),$token);
        return self::$version=="V1"?$tokenArr[0]:$tokenArr["id"];
    }
}
?>