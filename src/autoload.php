<?php
function xphp_autoload($classname){
    $rootpath=__DIR__.'/'.$classname.'.php';

    $rootpath=str_replace("Sonkwl\\xphp\\","",$rootpath);

    if(!file_exists($rootpath)){
        echo 'no file';
        return 0;
    }
    require_once $rootpath;
    return 1;
}
spl_autoload_register('xphp_autoload');
?>