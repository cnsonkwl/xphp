<?php
namespace Sonkwl\xphp;

class creater{
    public static function Project($dir){
        if(!is_dir($dir)) mkdir($dir,0777);
        if(!is_dir($dir."/controler")) mkdir($dir."/controler",0777);
        if(!is_dir($dir."/config")) mkdir($dir."/config",0777);
        if(!is_dir($dir."/cacher")) mkdir($dir."/cacher",0777);

        file_put_contents($dir."/config/router.php","<?php
use Sonkwl\xphp\\router;

router::Set([
    \"index\"=>[
        \"method\"=>\"GET\",
        \"priver\"=>\"Default\",
        \"middler\"=>\"\",
        \"cacher\"=>FALSE,
        \"dicter\"=>\"dicter\"
    ],
    \"index_add\"=>[
        \"method\"=>\"GET\",
        \"priver\"=>\"Admin\",
        \"middler\"=>\"\",
        \"cacher\"=>FALSE,
        \"dicter\"=>\"dicter\"
    ]
]);
?>");

        file_put_contents($dir."/config/dicter.php","<?php
use Sonkwl\xphp\dicter;

dicter::Set([
    \"p\"=>\"/^[0-9A-Za-z_]{1,20}$/\",
    \"token\"=>\"/^[0-9A-Za-z]{10,}$/\",
    \"s_no\"=>\"/^[0-9A-Za-z\u4e00-\u9fff]{4,20}$/\",
    \"s_name\"=>\"/^.{1,90}$/\",
    \"s_dept\"=>\"/^.{1,300}$/\",
    \"s_mail\"=>\"/^([a-zA-Z]|[0-9])(\w|\-)+@(mail.foxconn.com)|(foxconn.com)$/\",
    \"s_id\"=>\"/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/\",
    \"mail\"=>\"/^([a-zA-Z0-9]+[_|_|\-|.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|_|.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,6}$/\",
    \"s_date\"=>\"/^\d{4}\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|3[0-1])$/\",
    \"s_mobile\"=>\"/(^\d{11}$)|(^([6|9])\d{7}$)|(^[6]([8|6])\d{5})/\"
]);
?>");
        file_put_contents($dir."/config/config.php","<?php
use Sonkwl\xphp\config;

config::Set([
    \"dbname\"=>\"wh\",
    \"host\"=>\"wh\",
    \"user\"=>\"wh\",
    \"pwo\"=>\"wh\"
]);
?>");
        file_put_contents($dir."/controler/priver.php","<?php
class priver{
    static public function Default(){
        echo 'Default priv';
    }

    static public function Admin(){
        echo 'admin priv';
    }

    static public function User(){
        
    }
}
?>");
        file_put_contents($dir."/index.php","<?php
include \"../src/autoload.php\";

use Sonkwl\xphp\apper;

apper::Run();
?>");
        echo "OK";
    }
}
?>