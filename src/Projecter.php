<?php
namespace Sonkwl\xphp;
use Sonkwl\xphp\Starter;

class Projecter{
    //初始化项目
    public static function init($path){
        $files=[
            str_replace("src","",__DIR__)."docs/data/case.json",
            str_replace("src","",__DIR__)."docs/data/db.json",
            str_replace("src","",__DIR__)."docs/data/interface.json",
            str_replace("src","",__DIR__)."docs/data/info.json",
            str_replace("src","",__DIR__)."docs/index.php"
        ];

        if(!is_file("php/docs/data/case.json")){
            if(!is_dir("php")){
                mkdir("php",0777);
            }
    
            if(!is_dir("php/docs")){
                mkdir("php/docs",0777);
            }
            if(!is_dir("php/docs/data")){
                mkdir("php/docs/data",0777);
            }
    
            file_put_contents("php/docs/data/case.json",file_get_contents($files[0]));
            file_put_contents("php/docs/data/db.json",file_get_contents($files[1]));
            file_put_contents("php/docs/data/interface.json",file_get_contents($files[2]));
            file_put_contents("php/docs/data/info.json",str_replace("\$url\$",$path,file_get_contents($files[3])));
            file_put_contents("php/docs/index.php",file_get_contents($files[4]));
        }

        $info=json_decode(file_get_contents("php/docs/data/info.json"),true);
        if(!is_array($info)){
            echo 'data/info.json文件格式错误';
        }

        //* 新增发布功能
        $release="<?php\n";
        $release.="include_once \$_SERVER['DOCUMENT_ROOT'].'/publishphp/src/client.php';\n";
        $release.="use \Sonkwl\PublishPHP\client;\n";
        $release.="client::SetUrl('http://10.202.8.214/release-2024.php');\n";
        $release.="client::SetProjectName('".$info["project"]."');\n";
        $release.="client::SetNoDirs(array('php/docs'));\n";
        $release.="client::Release();\n";
        $release.="?>";
        file_put_contents("release-dev.php",$release);
        
        //* 新增php/configer/config.php
        if(!is_dir("php/configer")){
            mkdir("php/configer",0777);
        }
        $txt="<?php\n";
        $txt.="use Sonkwl\xphp\Configer;\n";
        $txt.="Configer::init([\n";
        $txt.="\t\"dbname\"=>\"wh\",\n";
        $txt.="\t\"host\"=>\"wh\",\n";
        $txt.="\t\"user\"=>\"wh\",\n";
        $txt.="\t\"pwd\"=>\"wh\",\n";
        $txt.="\t\"upload_url\"=>\"wh\",\n";
        $txt.="\t\"upload_path\"=>\"wh\",\n";
        $txt.="\t\"jwt_exp\"=>strtotime(\"+8 hours\")\n";
        $txt.="]);\n";
        $txt.="?>";
        file_put_contents("php/configer/config.php",$txt);
        
        //* 新增php/auther/default.php
        if(!is_dir("php/auther")){
            mkdir("php/auther",0777);
        }
        $txt="<?php\n";
        $txt.="?>\n";
        file_put_contents("php/auther/default.php",$txt);
        
        //* 新增php/uplader/default.php
        if(!is_dir("php/uploader")){
            mkdir("php/uploader",0777);
        }
        $txt="<?php\n";
        $txt.="?>\n";
        file_put_contents("php/uploader/default.php",$txt);
        
        //* 新增php/index.php
        $txt="<?php\n";
        $txt.="include_once \$_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';\n";
        $txt.="use Sonkwl\xphp\Starter;\n";
        $txt.="Starter::run();\n";
        $txt.="?>\n";
        file_put_contents("php/index.php",$txt);

        echo getcwd();
        exec("start http://localhost/".$path."/php/docs/index.php");
    }
}
?>