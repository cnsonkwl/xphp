<?php
namespace Sonkwl\xphp;

use Sonkwl\xphp\apper;

class router{
    static private $routes=[];
    public static function Set($r){
        self::$routes=$r;
    }
    public static function Get($key){
        if(!isset(self::$routes[$key])){
            apper::Response($res="NG",$message="config/router.php中{$key}没有配置");
        }
        return self::$routes[$key];
    }
    public static function Run(){
        if(!is_file("config/router.php")){
            apper::Response($res="NG",$message="config/router.php不存在");
        }
        include_once "config/router.php";
        if(count(self::$routes)==0){
            apper::Response($res="NG",$message="router定义为空");
        }
    }
}
?>