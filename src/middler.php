<?php
namespace Sonkwl\xphp;

class middler{
    public static function Run($page){
        $func=(router::Get($page))["middler"];
        if($func=="" || $func==false){
            return;
        }

        if(!is_file("controler/middler.php")){
            apper::Response($res="NG",$message="controler/middler.php不存在");
        }
        include_once "controler/middler.php";

        if(!method_exists("middler",$func)){
            apper::Response($res="NG",$message="controler/middler.php中[{$func}]不存在");
        }
        call_user_func("middler::{$func}");
    }
}
?>