<?php
namespace Sonkwl\xphp;

class priver{
    static private $data=[];
    static public function Get(){
        return self::$data;
    }
    static public function Set($val){
        self::$data=$val;
    }
    public static function Run($page){
        $func=(router::Get($page))["priver"];
        if($func=="" || $func==false){
            $func="Default";
        }

        if(!is_file("controler/priver.php")){
            apper::Response($res="NG",$message="controler/priver.php不存在");
        }
        include_once "controler/priver.php";

        if(!method_exists("priver",$func)){
            apper::Response($res="NG",$message="controler/priver.php中[{$func}]不存在");
        }
        call_user_func("priver::{$func}");
        self::$data=call_user_func("priver::Get");
    }
}
?>