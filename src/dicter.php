<?php
namespace Sonkwl\xphp;

use Sonkwl\xphp\apper;
use Sonkwl\xphp\router;

class dicter{
    private static $dict=[];

    public static function Set($val){
        self::$dict=$val;
    }
    public static function Get($key){
        if(!isset(self::$dict[$key])){
            apper::Response($res="NG",$message="dicter中{$key}没有配置");
        }
        return self::$dict[$key];
    }

    public static function Run($page){
        if(!is_file("config/".(router::Get($page))["dicter"].".php")){
            apper::Response($res="NG",$message="config/".(router::Get($page))["dicter"].".php 不存在");
        }
        include_once "config/".(router::Get($page))["dicter"].".php";

        $arr=$_SERVER["REQUEST_METHOD"]=="POST"?$_POST:$_GET;
        self::Check($arr);
    }

    public static function Check($arr){
        foreach ($arr as $key => $value) {
            if(is_array($value)){
                foreach($value as $key2=>$value2){
                    if(!preg_match(self::Get($key),$value2,$match)){
                        apper::Response($res="NG",$message="{$key}(数组)格式不合法");
                    }
                }
                continue;
            }
            if(!preg_match(self::Get($key),$value,$match)){
                apper::Response($res="NG",$message="{$key}的格式不合法");
            }
        }
    }
}
?>