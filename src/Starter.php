<?php
namespace Sonkwl\xphp;

use Sonkwl\xphp\Configer;
use Sonkwl\xphp\Validater;
use Sonkwl\xphp\Auther;
use Sonkwl\xphp\Controller;
use Sonkwl\xphp\Uploader;

class Starter{

    /**
     * 返回response
     */
    public static function response($res="OK",$message="",$data=[]){
        echo json_encode([
            "res"=>$res,
            "message"=>$message,
            "data"=>$data
        ],JSON_UNESCAPED_UNICODE);
        exit;
    }

    /**
     * 运行框架
     */
    public static function run(){
        $method=strtolower($_SERVER["REQUEST_METHOD"]);
        if(!isset($_GET["p"])){
            self::response("NG","入口错误，没有定义p");
        }
        $pages=explode("_",$_GET["p"]);
        if(count($pages)!=2){
            self::response("NG","入口错误，p必须是class_func模式");
        }
        $class=$pages[0];
        $func=$method."_".$pages[1];

        //载入Configer
        Configer::load();

        //载入Jwtter
        Jwtter::load();

        //载入Uploader
        Uploader::load($method,$_GET["p"]);

        //载入Validater
        Validater::load($method,$_GET["p"]);

        //载入Auther
        Auther::load($method,$_GET["p"]);

        //载入Controller
        Controller::load($class,$func);
    }
}
?>