<?php
if(!isset($_POST["token"])){
	echo '{"res":"NG","message":"token没有定义"}';
	exit;
}
if(!preg_match("/^.{1,300}$/",$_POST["token"])){
	echo '{"res":"NG","message":"token的格式不符合规定"}';
	exit;
}
if(!isset($_POST["s_file"])){
	echo '{"res":"NG","message":"s_file没有定义"}';
	exit;
}
if(!preg_match("/^.{1,300}$/",$_POST["s_file"])){
	echo '{"res":"NG","message":"文件路径的格式不符合规定"}';
	exit;
}
?>