<?php
namespace Sonkwl\xphp;

class apper{
    private static $router=[];
    private static $reponseType="JSON"; //JSON/TXT

    public static function Request(){
        try{
            $page="index";
            $method=strtolower($_SERVER["REQUEST_METHOD"]);
            if(isset($_GET["p"])){
                $page=$_GET["p"];
            }
            if(!isset(self::$router[$page])){
                //错误处理
                self::Response($res="NG",$message="没有定义{$page}");
            }
            //处理文件
            if(!is_file("controler/{$method}/{$page}.php")){
                self::Response($res="NG",$message="没有找到{$page}处理文件");
            }
            //权限处理
            if(is_file(self::$router[$page]["priver"].".php")){
                include_once self::$router[$page]["priver"].".php";
            }

            //中间件处理
            if(is_file(self::$router[$page]["middler"].".php")){
                include_once self::$router[$page]["middler"].".php";
            }

            //参数过滤
            if(is_file(self::$router[$page]["filter"].".php")){
                include_once self::$router[$page]["filter"].".php";
                self::Filter($method=="post"?$_POST:$_GET,$dicter);
            }

            if($method=="get"){
                //处理GET缓存
                self::HandlerCacher($method,$page);
            }
            include_once "controler/{$method}/{$page}.php";
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public static function GetExpTime($cacher){
        if(!preg_match('/^\d+/',$cacher,$metch)){
            self::Response($res="NG",$message="页面缓存设置时间异常");
        }
        $num=$metch[0];
        if(strpos($cacher,"minutes")!==FALSE){
            return time()-$num*60;
        }
        if(strpos($cacher,"hours")!==FALSE){
            return time()-$num*3600;
        }
        if(strpos($cacher,"days")!==FALSE){
            return time()-$num*3600*24;
        }
        return time()-$num;
    }

    public static function HandlerCacher($method,$page){
        if(self::$router[$page]["cacher"]==FALSE){
            return;
        }
        $cachefile=md5($_SERVER['REQUEST_URI']);
        if(!is_file("cacher/{$cachefile}.cache") || filemtime("cacher/{$cachefile}.cache")<self::GetExpTime(self::$router[$page]["cacher"])){
            ob_start();
            include_once "controler/{$method}/{$page}.php";
            $output=ob_get_contents();
            file_put_contents("cacher/{$cachefile}.cache",$output);
            ob_end_clean();
        }
        echo file_get_contents("cacher/{$cachefile}.cache");
        exit;
        //缓存处理
    }

    public static function Response($res="OK",$message="",$data=[]){
        switch (self::$reponseType) {
            // case 'JSON':
            //     echo json_decode($arr,JSON_UNESCAPED_UNICODE);
            //     break;
            case 'TXT':
                echo "{$res},{$message}";
                break;
            default:
                echo json_encode([
                    "res"=>$res,
                    "message"=>$message,
                    "data"=>$data
                ],JSON_UNESCAPED_UNICODE);
                break;
        }
        exit;
    }

    public static function Run($router=[],$reponseType="JSON"){
        self::$router=$router;
        self::$reponseType=$reponseType;
        self::Request();
    }

    public static function Filter($arr,$dicter){
        foreach($arr as $key=>$value){
            if(!isset($dicter[$key])){
                self::Response($res="NG",$message="{$key}没有校验配置");
            }
            //数组处理
            if(is_array($value)){
                foreach($value as $key2=>$value2){
                    if(!preg_match($dicter[$key],$value2,$match)){
                        self::Response($res="NG",$message="{$key}(数组)格式不合法");
                    }
                }
                continue;
            }
            if(!preg_match($dicter[$key],$value,$match)){
                self::Response($res="NG",$message="{$key}格式不合法");
            }
        }
    }
}
?>