<?php
namespace Sonkwl\xphp;
//创建项目脚本
class creater{
    public static function Project($dir){
        if(!is_dir($dir)) mkdir($dir,0777);
        if(!is_dir($dir."/cacher")) mkdir($dir."/cacher",0777);
        if(!is_dir($dir."/controler")) mkdir($dir."/controler",0777);
        if(!is_dir($dir."/controler/get")) mkdir($dir."/controler/get",0777);
        if(!is_dir($dir."/controler/post")) mkdir($dir."/controler/post",0777);
        if(!is_dir($dir."/dicter")) mkdir($dir."/dicter",0777);
        if(!is_dir($dir."/middler")) mkdir($dir."/middler",0777);
        if(!is_dir($dir."/router")) mkdir($dir."/router",0777);
        if(!is_dir($dir."/config"))  mkdir($dir."/config",0777);

        file_put_contents($dir."/dicter/default.php","<?php
\$dicter=[
    \"p\"=>\"/^[0-9A-Za-z_]{1,20}$/\",
    \"token\"=>\"/^[0-9A-Za-z]{10,}$/\",
    \"s_no\"=>\"/^[0-9A-Za-z\u4e00-\u9fff]{4,20}$/\",
    \"s_name\"=>\"/^.{1,90}$/\",
    \"s_dept\"=>\"/^.{1,300}$/\",
    \"s_mail\"=>\"/^([a-zA-Z]|[0-9])(\w|\-)+@(mail.foxconn.com)|(foxconn.com)$/\",
    \"s_id\"=>\"/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/\",
    \"mail\"=>\"/^([a-zA-Z0-9]+[_|_|\-|.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|_|.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,6}$/\",
    \"s_date\"=>\"/^\d{4}\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|3[0-1])$/\",
    \"s_mobile\"=>\"/(^\d{11}$)|(^([6|9])\d{7}$)|(^[6]([8|6])\d{5})/\"
];
?>");
        file_put_contents($dir."/router/router.php","<?php
\$routes=[
    \"index\"=>[
        \"method\"=>\"GET\",
        \"priver\"=>\"controler/priver\",
        \"cacher\"=>\"5minutes\",
        \"filter\"=>\"dicter/default\",
        \"middler\"=>\"middler/ipcontrol\"
    ]
];
?>");
        file_put_contents($dir."/config/config.php","<?php
define(\"DBIP\",\"localhost\");
define(\"DBUSER\",\"root\");
define(\"DBPWD\",\"root\");
define(\"DBNAME\",\"xczx\");
?>");
        file_put_contents($dir."/index.php","<?php
include 'router/router.php';
include 'config/config.php';
include '../src/autoload.php';

use Sonkwl\xphp\apper;

apper::Run(\$router=\$routes);
?>");
        echo "创建成功";
    }
}
?>