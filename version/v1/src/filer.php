<?php
namespace Sonkwl\xphp;

use Sonkwl\xphp\apper;

class filer{
    private static $url=NULL;
    private static $path="files";
    private static $type=["image/png","image/jpg","image/jpeg","image/gif","application/pdf","application/vnd.ms-excel","application/vnd.ms-powerpoint","application/msword","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"];//支持文件类型
    private static $size="20";
    private static $tag="default";

    //默认设置文件类型
    public static function Set_type_append($val){
        self::$type=array_merge(self::$type,$val);
    }
    public static function Set_type_image(){
        self::$type=["image/png","image/jpg","image/jpeg","image/gif"];
    }
    public static function Set_type_pdf(){
        self::$type=["application/pdf"];
    }
    public static function Set_type_office(){
        self::$type=["application/vnd.ms-excel","application/vnd.ms-powerpoint","application/msword","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"];
    }

    public static function Set($key,$value){
        self::$$key=$value;
    }
    public static function Get($key){
        if($key=="url"){
            return trim(self::$url,"/")."/";
        }
        if($key=="path"){
            self::$path=trim(self::$path,"/");
            if(!is_dir(self::$path)){
                if(!mkdir(self::$path,0777,true)){
                    apper::Response($res="NG",$message="创建文件目录失败");
                }
            }
            if(!is_dir(self::$path.'/'.date("Ymd"))){
                if(!mkdir(self::$path.'/'.date("Ymd"),0777,true)){
                    apper::Response($res="NG",$message="创建日期目录失败");
                }
            }
            return self::$path.'/'.date("Ymd")."/";
        }
        if($key=="type"){
            if(count(self::$type)==0 || !is_array(self::$type)){
                apper::Response($res="NG",$message="定义文件MIME错误");
            }
        }
        if($key=="size"){
            if(!is_numeric(self::$size)){
                apper::Response($res="NG",$message="定义文件SIZE错误");
            }
            return self::$size*1024*1024;
        }
        return self::$$key;
    }

    private static $typeTofile=[
        "image/png"=>"png",
        "image/jpg"=>"jpg",
        "image/jpeg"=>"jpeg",
        "image/gif"=>"gif",
        "application/pdf"=>"pdf",
        "application/vnd.ms-excel"=>"xls",
        "application/vnd.ms-powerpoint"=>"doc",
        "application/msword"=>"ppt",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"=>"xlsx"
    ];
    //获得文件后缀
    public static function Get_FileType($type){
        if(!isset(self::$typeTofile[$type])){
            apper::Response($res="NG",$message="文件格式查询失败[{$type}]");
        }
        return self::$typeTofile[$type];
    }

    //获得文件名
    public static function GetFileName($fileType,$fileSize){
        if(!in_array($fileType,self::Get("type"))){
            apper::Response($res="NG",$message="上传文件格式错误,请上传[".implode(",",self::Get("type"))."]文件");
        }
        if($fileSize>self::Get("size")){
            apper::Response($res="NG",$message="文件不能大于".int($self::Get("size")/1024/1024)."M");
        }
        return self::Get("path").self::Get("tag").date("YmdHis").random_int(1,10000).".".self::Get_FileType($fileType);
    }
    //$_FILES
    public static function SaveFileArray($file){
        try {
            $filePath=self::GetFileName($file["type"],$file["size"]);
            move_uploaded_file($file["tmp_name"],$filePath);
            return self::Get("url").$filePath;
        }catch (\Throwable $th) {
            apper::Response($res="NG",$message="文件保存失败");
        }
    }
    //filedata
    public static function SaveFileData($file){
        preg_match('/data:(\w+?\/\w+?);base64,(.+)$/si',$file,$result);
        if(count($result)!=3){
            return $file;
        }
        $filePath=self::GetFileName($result[1],intval(strlen($result[2]))/1024/1024);
        file_put_contents($filePath,base64_decode($result[2]));
        return self::Get("url").$filePath;
    }

    //操作文件，并将设置成post
    public static function FileToPost($file,$key){
        if(is_array($file) && isset($file["name"])){
            $_POST[$key] = self::SaveFileArray($file);
        }else{
            $_POST[$key] = self::SaveFileData($file);
        }
    }
}
?>